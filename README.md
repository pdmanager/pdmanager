# README #

In order to deploy in a server the PDManager web solution, the following steps shall be done.
It is relevant to state that each step may lead to different results depending on the OS of the machine this guideline is followed but,
in a nutshell, everything should work fine even when some roundabouts may be required. We recommend for this version, to operate under
a windows OS as the latest features of the project are managing the filsesystem and using a shell command named 7z (which has an equivalen for Linux)
This binary shall be added to the path of the system so it can be invoked anytime and in any leaf of the filesystem tree.
The program 7z can be obtained in: http://www.7-zip.org/download.html

First, create a folder of your choice, like "pdmanager" in your file system.
It is recommended to avoid spaces in all the folders pointing to that path you have just created.
Open a shell and navigate to the recently created target path (entering it).
Make sure you have installed and its included in the path of the OS the git tool: https://git-scm.com/
Enter the following commands:

-> git clone https://miguelpcm@bitbucket.org/pdmanager/pdmanager.git

Install Python v 3.x in your system. (3.5.1 is the recommended version)
In the website https://www.python.org/downloads/ you can obtain the installer for windows.
in linux, depending on your distro, the following command should work:
-> sudo apt-get install python3.4

Dont foget to include the path to the python binaries in your windows path.
You can confirm python has been properly set up by typing in a shell:
-> python --version
or
-> python3 --version
In case you binary has been named python3, you should invoke it that way or change its alias (in linux) so you can
call it just without the ending "3".
The previous commands should return something like: "Python 3.4.2"


Among the python installation, it should have been included other tool named pip.
Install django with the following command:
-> pip install Django==1.9.8
If you need to update pip to its latest version just type 
-> pip install --upgrade pip
or the following can work as well
-> python -m pip install --upgrade pip

Now, inside the directory where the project was cloned, you should find a directory named "pdamanager".
inside it it should appear a file named manage.py and a directory named pdManagerCloud.
Enter the path pdManagerCloud and from here, go to the directory named Scripts

-> once in tour_path\pdmanager\pdManagerCloud\Scripts

just execute from the shell the file named activate.bat (future guidelines will be made for linux)
after that, go back to the directory your_path\pdmanager\ and type the following command:
-> python manage.py runserver

You should have an instance of a django server running in localhost port 8080.
Now open any browser of your choice (chrome is recommended over firefox) and navigate to localhost: 127.0.0.1:8080
The login page should appear, enter as both user, pass the following credential: pdmanager

Done