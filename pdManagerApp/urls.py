from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import views

urlpatterns = [
    url(r'^$', views.dashboard, name='dashboard'),
    url(r'^guide/$', views.guide, name='guide'),
    url(r'^professional/profile/(?P<pk>[0-9]+)/(?P<pp>[0-9]+)/$', views.profile_detail, name='profile_detail'),
    url(r'^professional/profile/(?P<pk>[0-9]+)/(?P<pp>[0-9]+)/(?P<t1>[0-9]+)/$', views.session_update, name='profile_detail'),
    url(r'^session/save/(?P<pk>[0-9]+)/(?P<pp>[0-9]+)/(?P<t1>[0-9]+)/$', views.session_update, name='profile_detail'),
    url(r'^session/save/$', views.session_update, name='session_save'),
    url(r'^professional/explore_data/$', views.explore_data, name='explore_data'),
    url(r'^professional/explore_data/(?P<pk>[0-9]+)/$', views.explore_data, name='explore_data'),
    url(r'^professional/explore_data/(?P<pk>[0-9]+)/show_data/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/$', views.check_data, name='show_data'),
    url(r'^professional/configure/$', views.configure, name='configure'),
    url(r'^professional/files/(?P<pk>[0-9]+)/$', views.files, name='files'),
    url(r'^professional/raw_data_cog/(?P<pk>[0-9]+)/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/(?P<hasSensorsData>[0-1]{1})/$', views.raw_data_cog, name='raw_data_cog'),
    url(r'^professional/raw_data_sens/(?P<pk>[0-9]+)/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/$', views.raw_data_sens, name='raw_data_sens'),
    url(r'^professional/aggregated_raw_data/(?P<pk>[0-9]+)/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/$', views.aggregated_raw_data, name='aggregated_raw_data'),

# User management
    url(r'^new_patient/$', views.new_patient, name='new_patient'),

# Files uploading
    url(r'zip_file_upload/$', views.zip_file_upload, name='zip_file_upload'),

# Sensors
    url(r'^annotation/$', views.annotation, name='annotation'),
    url(r'^accelerometer/$', views.accelerometer, name='accelerometer'),
    url(r'^orientation/$', views.orientation, name='orientation'),
    url(r'^acmcorrected/$', views.acmcorrected, name='acmcorrected'),
    url(r'^gyromobile/$', views.gyromobile, name='gyromobile'),
    url(r'^sensoracc/$', views.sensoracc, name='sensoracc'),
    url(r'^gyro/$', views.gyro, name='gyro'),
    url(r'^hr/$', views.hr, name='hr'),
    url(r'^st/$', views.st, name='st'),
    url(r'^pedometer/$', views.pedometer, name='pedometer'),

    url(r'^raccelerometer/$', views.raccelerometer, name='raccelerometer'),
    url(r'^rorientation/$', views.rorientation, name='rorientation'),
    url(r'^racmcorrected/$', views.racmcorrected, name='racmcorrected'),
    url(r'^rgyromobile/$', views.rgyromobile, name='rgyromobile'),
    url(r'^rsensoracc/$', views.rsensoracc, name='rsensoracc'),
    url(r'^rgyro/$', views.rgyro, name='rgyro'),
    url(r'^rhr/$', views.rhr, name='rhr'),
    url(r'^rst/$', views.rst, name='rst'),
    url(r'^rpedometer/$', views.rpedometer, name='rpedometer'),

# Cognitive
    url(r'^ftSimple/$', views.ftSimple, name='ftSimple'),
    url(r'^ftAlternate/$', views.ftAlternate, name='ftAlternate'),
    url(r'^pal/$', views.pal, name='pal'),
    url(r'^prm/$', views.prm, name='prm'),
    url(r'^swm/$', views.swm, name='swm'),
    url(r'^ssp/$', views.ssp, name='ssp'),
    url(r'^sst/$', views.sst, name='sst'),
    url(r'^ast/$', views.ast, name='ast'),
    url(r'^vas/$', views.vas, name='vas'),

]

urlpatterns += staticfiles_urlpatterns()