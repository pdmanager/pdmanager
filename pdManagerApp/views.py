from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from django.contrib.auth.views import password_reset, password_reset_confirm
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404, render_to_response, render
from django.db import IntegrityError
from .models import models, Patient, Professional, Session
from .forms import MyRegistrationForm, AddSession
from django.contrib.auth.models import User
from pdManagerCloud import settings
import os, csv, statistics, math, datetime, json
import rarfile


@login_required
def dashboard(request):
    patients = Patient.objects.all()
    patients_with_data_presence = []
    for p in patients:
        #if p.code.startswith("demo"): # or p.code=="PDMANAGER2":
        #    p.delete()
        #    User.objects.get(username=p.code).delete()
        #else:
            patients_with_data_presence.append({'patient': p, 'hasCognitive': hasCognitive(p.code), 'hasMotor': hasMotor(p.code)})
    return render(request, 'dashboard.html', {'patients': patients_with_data_presence })

def hasCognitive(code):
    patientDataPath = 'pdManagerApp' + os.sep + 'patients' + os.sep + code;
    if not os.path.exists(patientDataPath):
        os.makedirs(patientDataPath)
        return 'false'
    fullpath = os.path.join(settings.BASE_DIR, patientDataPath)
    files = [f for f in os.listdir(fullpath) if not os.path.isdir(os.path.join(fullpath, f))]
    if len(files) > 0:
        return 'true'
    else:
        return 'false'

def hasMotor(code):
    patientDataPath = 'pdManagerApp' + os.sep + 'patients' + os.sep + code;
    if not os.path.exists(patientDataPath):
        os.makedirs(patientDataPath)
        return 'false'
    fullpath = os.path.join(settings.BASE_DIR, patientDataPath)
    folders = [f for f in os.listdir(fullpath) if os.path.isdir(os.path.join(fullpath, f))]
    if len(folders) > 0:
        return 'true'
    else:
        return 'false'

def handle_uploaded_zip(file, code):
    path = 'pdManagerApp' + os.sep + 'temp_uploads' + os.sep + file.name
    target = 'pdManagerApp' + os.sep + 'patients' + os.sep + code
    with open(path, 'wb+') as dest:
        for chunk in file.chunks():
            dest.write(chunk)
        dest.close()
    rf = rarfile.RarFile(path)
    rf.extractall(target)

# Zip file upload
@login_required
def zip_file_upload(request):
    if request.method=='POST':
        f = request.FILES.get('file')
        code = request.POST.get('code')
        handle_uploaded_zip(f, code)
        return HttpResponse('{}', content_type='application/json')

# New patient
@login_required
def new_patient(request):
    mcode = request.GET.get('code', '')
    mprofile = request.GET.get('profile', '')
    patients = Patient.objects.all()
    for p in patients:
        if p.code == mcode:
            return HttpResponse(json.dumps('false'), content_type='application/json')
    try:
        uid = User.objects.create_user(username=mcode)
    except(IntegrityError):
        return HttpResponse(json.dumps('false'), content_type='application/json')
    patient = Patient(user=uid, code=mcode, profile=mprofile)
    patient.save()
    patientDataPath = 'pdManagerApp' + os.sep + 'patients' + os.sep + p.code;
    if not os.path.exists(patientDataPath):
        os.makedirs(patientDataPath)
    return HttpResponse(json.dumps('true'), content_type='application/json')

def processAjaxRequest(request):
    pk = request.GET.get('pk', '')
    year = request.GET.get('year', '')
    month = request.GET.get('month', '')
    day = request.GET.get('day', '')
    p = get_object_or_404(Patient, pk=pk)
    patientDataPath = 'pdManagerApp' + os.sep + 'patients' + os.sep + p.code;
    patientDataFullPath = os.path.join(settings.BASE_DIR, patientDataPath)
    date = [year, day, month]
    date = list(map(int, date))
    return [patientDataFullPath, date]

# -- Sensor Ajax Calls

def raw_sensor(request, file):
    processedRequest = processAjaxRequest(request)
    patientDataFullPath = processedRequest[0]
    date = processedRequest[1]
    data = read_data_sensor(patientDataFullPath, date, file)
    return HttpResponse(json.dumps(data), content_type='application/json')

def annotation(request):
    return raw_sensor(request, 'annot.txt')


def accelerometer(request):
    processedRequest = processAjaxRequest(request)
    patientDataFullPath = processedRequest[0]
    date = processedRequest[1]
    resultants = resultants_data_sensor(patientDataFullPath, date, 'sensor_acc_mobile.txt', 3)
    data = statistics_data_sensor(resultants, date)
    return HttpResponse(json.dumps(data), content_type='application/json')

def raccelerometer(request):
    return raw_sensor(request, 'sensor_acc_mobile.txt')


def orientation(request):
    processedRequest = processAjaxRequest(request)
    patientDataFullPath = processedRequest[0]
    date = processedRequest[1]
    resultants = resultants_data_sensor(patientDataFullPath, date, 'sensor_orient_mobile.txt', 3)
    data = statistics_data_sensor(resultants, date)
    return HttpResponse(json.dumps(data), content_type='application/json')

def rorientation(request):
    return raw_sensor(request, 'sensor_orient_mobile.txt')


def acmcorrected(request):
    processedRequest = processAjaxRequest(request)
    patientDataFullPath = processedRequest[0]
    date = processedRequest[1]
    resultants = resultants_data_sensor(patientDataFullPath, date, 'sensor_acc_corr_mobile.txt', 3)
    data = statistics_data_sensor(resultants, date)
    return HttpResponse(json.dumps(data), content_type='application/json')

def racmcorrected(request):
    return raw_sensor(request, 'sensor_acc_corr_mobile.txt')


def gyromobile(request):
    processedRequest = processAjaxRequest(request)
    patientDataFullPath = processedRequest[0]
    date = processedRequest[1]
    resultants = resultants_data_sensor(patientDataFullPath, date, 'sensor_gyro_mobile.txt', 3)
    data = statistics_data_sensor(resultants, date)
    return HttpResponse(json.dumps(data), content_type='application/json')

def rgyromobile(request):
    return raw_sensor(request, 'sensor_gyro_mobile.txt')


def sensoracc(request):
    processedRequest = processAjaxRequest(request)
    patientDataFullPath = processedRequest[0]
    date = processedRequest[1]
    resultants = resultants_data_sensor(patientDataFullPath, date, 'sensor_acc.txt', 3)
    data = statistics_data_sensor(resultants, date)
    return HttpResponse(json.dumps(data), content_type='application/json')

def rsensoracc(request):
    return raw_sensor(request, 'sensor_acc.txt')


def gyro(request):
    processedRequest = processAjaxRequest(request)
    patientDataFullPath = processedRequest[0]
    date = processedRequest[1]
    resultants = resultants_data_sensor(patientDataFullPath, date, 'sensor_gyro.txt', 3)
    data = statistics_data_sensor(resultants, date)
    return HttpResponse(json.dumps(data), content_type='application/json')

def rgyro(request):
    return raw_sensor(request, 'sensor_gyro.txt')


def hr(request):
    processedRequest = processAjaxRequest(request)
    patientDataFullPath = processedRequest[0]
    date = processedRequest[1]
    resultants = resultants_data_sensor(patientDataFullPath, date, 'sensor_hr.txt', 2)
    data = statistics_data_sensor(resultants, date)
    return HttpResponse(json.dumps(data), content_type='application/json')

def rhr(request):
    return raw_sensor(request, 'sensor_hr.txt')


def st(request):
    processedRequest = processAjaxRequest(request)
    patientDataFullPath = processedRequest[0]
    date = processedRequest[1]
    resultants = resultants_data_sensor(patientDataFullPath, date, 'sensor_st.txt', 1)
    data = statistics_data_sensor(resultants, date)
    return HttpResponse(json.dumps(data), content_type='application/json')

def rst(request):
    return raw_sensor(request, 'sensor_st.txt')


def pedometer(request):
    processedRequest = processAjaxRequest(request)
    patientDataFullPath = processedRequest[0]
    date = processedRequest[1]
    resultants = resultants_data_sensor(patientDataFullPath, date, 'pedo.txt', 1)
    data = statistics_data_sensor(resultants, date)
    return HttpResponse(json.dumps(data), content_type='application/json')

def rpedometer(request):
    return raw_sensor(request, 'pedo.txt')


# -- Cognitive Ajax Calls
def cognitive(request, fName):
    processedRequest = processAjaxRequest(request)
    patientDataFullPath = processedRequest[0]
    date = processedRequest[1]
    return read_data_cognitive(patientDataFullPath, date, fName)

def ftSimple(request):
    data = cognitive(request, 'FingerTappingSimple.csv')
    return HttpResponse(json.dumps(data), content_type='application/json')

def ftAlternate(request):
    data = cognitive(request, 'FingerTappingAlternate.csv')
    return HttpResponse(json.dumps(data), content_type='application/json')

def pal(request):
    data = cognitive(request, 'PAL_Results.csv')
    return HttpResponse(json.dumps(data), content_type='application/json')

def prm(request):
    data = cognitive(request, 'PRM_Results.csv')
    return HttpResponse(json.dumps(data), content_type='application/json')

def swm(request):
    data = cognitive(request, 'SWM_Results.csv')
    return HttpResponse(json.dumps(data), content_type='application/json')

def ssp(request):
    data = cognitive(request, 'SSP_Results.csv')
    return HttpResponse(json.dumps(data), content_type='application/json')

def sst(request):
    data = cognitive(request, 'SST_Results.csv')
    return HttpResponse(json.dumps(data), content_type='application/json')

def ast(request):
    data = cognitive(request, 'AST_Results.csv')
    return HttpResponse(json.dumps(data), content_type='application/json')

def vas(request):
    data = cognitive(request, 'VAS_Results.csv')
    return HttpResponse(json.dumps(data), content_type='application/json')

# -- Navigation and settings

def configure(request):
    return render(request, 'configure.html')

@login_required
def files(request, pk):
    p = get_object_or_404(Patient, pk=pk)
    return render(request, 'files.html', {'patient': p} )

def session_edit(request):
    if request.method == 'POST':
        form = AddSession(request.POST)
        if form.is_valid():
            return render_to_response('dashboard.html')
    token = {}
    token.update(csrf(request))
    token['form'] = form
    return render_to_response('dashboard.html', token)


def register(request):
    token = {}
    token.update(csrf(request))
    # When the user arrives in the page (not a POST) it loads MyRegistrationForm()
    # and shows register.html with the token
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return render_to_response('registration/registration_complete.html')
        else:
            token['form'] = form
            return render_to_response('registration/register.html', token)
    else:
        form = MyRegistrationForm()
    token['form'] = form
    return render_to_response('registration/register.html', token)


def registration_complete(request):
    return render_to_response('registration/registration_complete.html')


def reset_confirm(request, uidb64=None, token=None):
    return password_reset_confirm(request,
                                  template_name='registration/password_reset_update_pass.html',
                                  uidb64=uidb64,
                                  token=token,
                                  post_reset_redirect=reverse('reset_complete'))


def reset(request):
    return password_reset(request,
                          template_name='registration/password_reset_request.html',
                          email_template_name='registration/password_reset_custom_email.html',
                          subject_template_name='registration/password_reset_subject.txt',
                          post_reset_redirect=reverse('reset_success'))


def reset_success(request):
    return render(request, 'registration/password_reset_success.html')


def reset_complete(request):
    return render(request, 'registration/password_reset_finish.html')


@login_required
def profile_detail(request, pk, pp):
    profile = get_object_or_404(Patient, pk=pk)
    user = get_object_or_404(Professional, id=pp)

    try:
        session = Session.objects.filter(patient=profile).latest('created_date')
    except:
        session = Session(patient=profile, professional=user, phase_I_duration=5, phase_II_duration=5,
                          phase_III_duration=5, phase_IV_duration=5, phase_V_duration=5, min_BPM=80, max_BPM=140)
        session.save()

    # check if this professional has access to the data of the patient
    return render(request, 'professional/profile.html', {'patient': profile, 'professional': user, 'session': session})


@login_required
def session_update(request):
    if request.method == 'POST':

        # POST goes here . is_ajax is must to capture ajax requests. Beginner's pit.
        if request.is_ajax():
            # Always use get on request.POST. Correct way of querying a QueryDict.
            pk = request.POST.get('pk')
            pp = request.POST.get('pp')
            t1 = request.POST.get('t1')
            t2 = request.POST.get('t2')
            t3 = request.POST.get('t3')
            t4 = request.POST.get('t4')
            t5 = request.POST.get('t5')
            max = request.POST.get('max')
            min = request.POST.get('min')

            print("Track 1 t1: " + t1)
            patient = get_object_or_404(Patient, pk=pk)
            professional = get_object_or_404(Professional, pk=pp)

            print("Track 2 t1: " + t1)
            session = Session(patient=patient, professional=professional, phase_I_duration=t1, phase_II_duration=t2,
                              phase_III_duration=t3, phase_IV_duration=t4, phase_V_duration=t5, min_BPM=min,
                              max_BPM=max)
            session.save()

            print("Track 3 t1: " + t1)

            data = {"pk": pk, "pp": pp}
            # Returning same data back to browser.It is not possible with Normal submit
            return JsonResponse(data)
    # Get goes here
    return render(request, 'professional/profile.html')


@login_required
def explore_data(request, pk):
    #BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    p = get_object_or_404(Patient, pk=pk)
    dataPath = 'pdManagerApp'+ os.sep + 'patients' + os.sep + p.code;
    #path = os.path.join(settings.BASE_DIR, dataPath)
    path = settings.BASE_DIR + os.sep + dataPath

    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    folders = [f for f in os.listdir(path) if os.path.isdir(os.path.join(path, f))]
    data_files = read_data_files(path, files)
    data_folders = [];
    for f in folders:
        for root, dirs, filelist in os.walk(os.path.join(path, f)):
            if len(dirs) != 0 or len(filelist) != 0:
                data_folders.append(f)
    return render(request, 'professional/explore_data.html', {'patient': pk, 'folders': data_folders, 'files': data_files})


def read_data_files(fullpath, filesname):
    dates = []
    for file in filesname:
        path = os.path.join(fullpath, file);
        with open(path, 'rt') as csvfile:
            try:
                reader = csv.reader(csvfile)
                next(reader, None)
                for row in reader:
                    date = str(row).strip('[]').split(",")[0]
                    dates.append(date)
            finally:
                csvfile.close()
    return dates


def guide(request):
    return render(request, 'guide.html')


def read_data_sensor(fullpath, date, filename):
    folders = [f for f in os.listdir(fullpath) if os.path.isdir(os.path.join(fullpath, f))]
    data = []
    for foldername in folders:
        folderTime = foldername.split('_')
        if folderMatchesDate(folderTime, date):
            filepath = os.path.join(fullpath, foldername, filename)
            if os.path.exists(filepath):
                data.append(folderTime)
                with open(filepath, 'rt') as content:
                    try:
                        reader = content.readlines()
                        for line in reader:
                            if (line[0]!='#'):
                                data.append(line.split())
                    finally:
                        content.close()
    return data


def read_data_cognitive(fullpath, date, filename):
    data = []
    month = date[2]
    if date[2]<10:
        month = '0' + str(month)
    day = date[1]
    if date[1] < 10:
        day = '0' + str(day)
    filterDate = str(date[0])+'-'+str(month)+'-'+str(day)
    filepath = os.path.join(fullpath, filename)
    if os.path.exists(filepath):
        with open(filepath, 'rt') as content:
            try:
                reader = csv.reader(content)
                for row in reader:
                    line = str(row).strip('[]').split("],")
                    timestamp = str(line).strip('[]').split(" ")[0]
                    if timestamp.split("'")[1] == filterDate:
                        data.append(line)
            finally:
                content.close()
    return data


@login_required
def aggregated_raw_data(request, pk, year, month, day):
    return render(request, 'professional/aggregated_raw_data.html',
              {'patient': pk, 'year': year, 'month': month, 'day': day})

@login_required
def raw_data_sens(request, pk, year, month, day):
    return render(request, 'professional/raw_data_sens.html',
              {'patient': pk, 'year': year, 'month': month, 'day': day})

@login_required
def raw_data_cog(request, pk, year, month, day, hasSensorsData):
    return render(request, 'professional/raw_data_cog.html',
              {'patient': pk, 'year': year, 'month': month, 'day': day, 'hasSensorsData': hasSensorsData})


@login_required
def check_data(request, pk, year, month, day):
    p = get_object_or_404(Patient, pk=pk)
    patientDataPath = 'pdManagerApp' + os.sep + 'patients' + os.sep + p.code;
    patientDataFullPath = os.path.join(settings.BASE_DIR, patientDataPath)
    date = [year, day, month]
    date = list(map(int, date))
    existsDataSensors = exists_data_sensor(patientDataFullPath, date)
    if existsDataSensors:
        return render(request, 'professional/aggregated_raw_data.html',
                  {'patient': pk, 'year': year, 'month': month, 'day': day})
    else:
        return render(request, 'professional/raw_data_cog.html',
                  {'patient': pk, 'year': year, 'month': month, 'day': day, 'hasSensorsData': 0})


def folderMatchesDate(folderTime, date):
    return int(folderTime[0])==date[0] and int(folderTime[1])==date[1] and int(folderTime[2])==date[2]


def resultants_data_sensor(fullpath, date, filename, nChannels):
    folders = [f for f in os.listdir(fullpath) if os.path.isdir(os.path.join(fullpath, f))]
    resultants = []
    for foldername in folders:
        dayTime = foldername.split("_")
        if folderMatchesDate(dayTime, date):
            path = os.path.join(fullpath, foldername, filename)
            if os.path.exists(path):
                #daySecs = dayTime[5].split(".") # guardamos en la 5 pos el split de s y ms
                timestamp = datetime.datetime(int(dayTime[0]), int(dayTime[2]), int(dayTime[1]), int(dayTime[3]),
                    int(dayTime[4]), int(dayTime[5]), 0)
                with open(path, 'rt') as content:
                    try:
                        reader = content.readlines()
                        for line in reader:
                            if (line[0]!='#'):
                                values = list(map(float, line.split()))
                                sum = 0
                                time = values[0]
                                resultant_ts = timestamp + datetime.timedelta(seconds=time)
                                if (nChannels==1):
                                    resultants.append([resultant_ts, values[1]])
                                else:
                                    for i in range (1, nChannels):
                                        sum = sum + math.pow(values[i], 2)
                                    resultant = math.sqrt(sum)
                                    resultants.append([resultant_ts, resultant])
                    finally:
                        content.close()
    return resultants


def statistics_data_sensor(resultants, date):
    if resultants==[]:
        return []
    else:
        data = [None] * 48 #48 30-minutes-intervals in a day
        endRange = datetime.datetime(date[0], date[2], date[1] + 1, 0, 0, 0, 0)
        # Las resultantes se distribuyen en slots en función de su tiempo
        for resultant in resultants:
            time = resultant[0]
            if time<endRange:
                posData = 2*time.hour
                if time.minute>29:
                    posData += 1
                if data[posData] is None:
                    data[posData] = [resultant[1]]
                else:
                    data[posData].append(resultant[1])
        # Ahora aplicamos los estadísticos
        res = []
        initRange = datetime.datetime(date[0], date[2], date[1], 0, 15, 0, 0)
        for i in range(0, 47):
            resSet = data[i]
            newDate = initRange + datetime.timedelta(minutes=i*30)
            ms = newDate.timestamp()*1000
            if resSet is not None:
                mean = statistics.mean(resSet)
                std = statistics.stdev(resSet)
                res.append([ms, mean, std])
        return res


def exists_data_sensor(fullpath, date):
    folders = [f for f in os.listdir(fullpath) if os.path.isdir(os.path.join(fullpath, f))]
    for foldername in folders:
        folderSegments = foldername.split("_")
        if folderMatchesDate(folderSegments, date):
            return True
    return False
