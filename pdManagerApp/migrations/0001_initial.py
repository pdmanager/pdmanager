# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CareLinks',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('approved_link', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('email', models.CharField(default='', max_length=200)),
                ('profile', models.CharField(default='Parkinson', max_length=200)),
                ('code', models.CharField(max_length=200, unique=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Professional',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('email', models.CharField(default='', max_length=200)),
                ('profile', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('phase_I_duration', models.CharField(max_length=200)),
                ('phase_II_duration', models.CharField(max_length=200)),
                ('phase_III_duration', models.CharField(max_length=200)),
                ('phase_IV_duration', models.CharField(max_length=200)),
                ('phase_V_duration', models.CharField(max_length=200)),
                ('min_BPM', models.CharField(default='80', max_length=200)),
                ('max_BPM', models.CharField(default='140', max_length=200)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('patient', models.ForeignKey(to='pdManagerApp.Patient', related_name='sessions')),
                ('professional', models.ForeignKey(to='pdManagerApp.Professional', related_name='sessions')),
            ],
        ),
        migrations.CreateModel(
            name='SessionFile',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('file', models.FileField(upload_to='uploads')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('patient', models.ForeignKey(to='pdManagerApp.Patient', related_name='session_files')),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=200)),
                ('text', models.TextField()),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.AddField(
            model_name='professional',
            name='team',
            field=models.ForeignKey(to='pdManagerApp.Team', related_name='professionals'),
        ),
        migrations.AddField(
            model_name='professional',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True),
        ),
        migrations.AddField(
            model_name='carelinks',
            name='patient',
            field=models.ForeignKey(to='pdManagerApp.Patient', related_name='patients'),
        ),
        migrations.AddField(
            model_name='carelinks',
            name='professional',
            field=models.ForeignKey(to='pdManagerApp.Professional', related_name='professionals'),
        ),
    ]
