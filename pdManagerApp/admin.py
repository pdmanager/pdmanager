from django.contrib import admin
from .models import CareLinks, Patient, Professional, Session, SessionFile, Team


# Register your models here.
admin.site.register(CareLinks)
admin.site.register(Patient)
admin.site.register(Professional)
admin.site.register(Session)
admin.site.register(SessionFile)
admin.site.register(Team)

